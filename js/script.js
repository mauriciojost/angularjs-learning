function FilterController($scope, $log) {
	$log.log('Creating customers...');
	$scope.customers  = [
		{ name: 'roque', city: 'obera' },
		{ name: 'betun', city: 'la granja' },
		{ name: 'mauri', city: 'jesus maria' },
		{ name: 'marta', city: 'messina' }
	];
	$log.log('Created.');
}
// $http is a service, described in 
// https://code.angularjs.org/1.2.26/docs/api/ng/service/$http
// $scope 

angular.module('myModule', [])
.controller(
	'RestController', [
		'$scope', 
		'$http',
		'$log', 
		function($scope, $http, $log) {
			$log.log('Executing get...');
			$http
				.get('http://rest-service.guides.spring.io/greeting')
				.success(
					function(data) {
						$scope.greeting = data;
					}
				);
			$log.log('Executed.');
		}
	] 
);

